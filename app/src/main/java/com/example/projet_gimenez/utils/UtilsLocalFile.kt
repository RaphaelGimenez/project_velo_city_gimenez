package com.example.projet_gimenez.utils

import android.content.Context
import java.io.File
import java.io.FileInputStream

object Utils {
    @JvmStatic
    fun writeToJSONFile(context: Context, folder: String, fileName: String, content: String) {

        // Get directory
        val filePath = context.filesDir
        val dir = File(filePath, folder)
        if (!dir.exists()) {
            dir.mkdir()
        }

        // Get file and remove it already exist
        val file = File(dir, "$fileName.json")
        if (file.exists()) {
            file.delete()
        }

        // write newly fetched data
        file.appendText(content)
    }

    @JvmStatic
    fun readFromJSONFile(context: Context, folder: String, fileName: String): String? {

        // Get folder directory
        val filePath = context.filesDir
        val dir = File(filePath, folder)
        if (!dir.exists()) {
            return null
        }

        // Get file
        val file = File(dir, "$fileName.json")
        if (!file.exists()) {
            return null
        }

        return FileInputStream(file).bufferedReader().use { it.readText() }
    }
}