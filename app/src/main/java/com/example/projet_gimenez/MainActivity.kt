package com.example.projet_gimenez

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import com.example.projet_gimenez.adapters.ContractAdapters
import com.example.projet_gimenez.adapters.CustomViewHolder
import com.example.projet_gimenez.models.Contract
import com.example.projet_gimenez.models.Station
import com.example.projet_gimenez.utils.Utils
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_main.*
import okhttp3.*
import java.io.*

class MainActivity : AppCompatActivity() {

    private val apiUrl: String = "https://api.jcdecaux.com/vls/v3"
    private val apiKey: String = "?apiKey=7886a12c53604b2668a08582a04795afcc9375b0"

    private var contracts: Array<Contract> = arrayOf()
    private var filteredContracts: Array<Contract> = arrayOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        recyclerView_main.layoutManager = GridLayoutManager(this, 2)

        getContracts()

        searchBar.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                // If a contract have multiple cities iterate through them
                val newFilteredContracts = contracts
                    .filter {
                        it.cities?.any { city -> city.contains(s, ignoreCase = true) }
                            ?: it.name.contains(s, ignoreCase = true)
                    }.toTypedArray()

                println(contracts.size)

                filteredContracts = newFilteredContracts

                runOnUiThread {
                    recyclerView_main.adapter = ContractAdapters(filteredContracts, ::getStations)
                }
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
    }

    private fun getContracts() {

        val url = "$apiUrl/contracts$apiKey"

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val bodyContent = response.body?.string()

                if (bodyContent !== null) {
                    setContracts(bodyContent, false)
                    Utils.writeToJSONFile(applicationContext, "project", "Contracts", bodyContent)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("No internet Access")
                val hasData = Utils.readFromJSONFile(applicationContext, "project", "Contracts")
                if (hasData !== null) {
                    setContracts(hasData, true)
                    println("local data found")
                } else {
                    println("no local data")
                }
            }
        })
    }

    private fun getStations(cityName: String): Boolean {
        layout_loading.visibility = View.VISIBLE

        val url = "$apiUrl/stations$apiKey&contract=$cityName"
        println(url)

        val request = Request.Builder().url(url).build()

        val client = OkHttpClient()
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val bodyContent = response.body?.string()

                if (bodyContent !== null) {
                    setStationActivity(bodyContent, false, cityName)
                    Utils.writeToJSONFile(applicationContext, "project", "Station_$cityName", bodyContent)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("No internet Access")
                val readData =
                    Utils.readFromJSONFile(applicationContext, "project", "Station_$cityName")
                if (readData !== null) {
                    setStationActivity(readData, true, cityName)
                    println("local data found")
                } else {
                    println("no local data")
                }
            }
        })

        return true
    }

    private fun setContracts(nextContracts: String, local: Boolean) {
        val gson = GsonBuilder().create()
        val nextContractsArray = gson.fromJson(nextContracts.trim(), Array<Contract>::class.java)
        contracts = nextContractsArray
        filteredContracts = nextContractsArray

        runOnUiThread {
            if (local) {
                Toast.makeText(
                    applicationContext,
                    "Can't reach server, using local data",
                    Toast.LENGTH_LONG
                ).show()
            }
            recyclerView_main.adapter = ContractAdapters(filteredContracts, ::getStations)
        }
    }

    private fun setStationActivity(cityStations: String, local: Boolean, cityName: String) {
        val gson = GsonBuilder().create()
        val stations = gson.fromJson(cityStations.trim(), Array<Station>::class.java)

        val intent = Intent(applicationContext, StationsActivity::class.java)
        intent.putExtra(CustomViewHolder.CITY_NAME, cityName)

        intent.putExtra(CustomViewHolder.STATIONS, gson.toJson(stations))

        runOnUiThread {
            if (local) {
                Toast.makeText(
                    applicationContext,
                    "Last update: ${stations[0].lastUpdate}",
                    Toast.LENGTH_LONG
                ).show()
            }
            layout_loading.visibility = View.INVISIBLE
            this.startActivity(intent)
        }
    }
}