package com.example.projet_gimenez.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projet_gimenez.R
import com.example.projet_gimenez.models.Station
import kotlinx.android.synthetic.main.station_search_item.view.*
import org.osmdroid.api.IMapController
import org.osmdroid.util.GeoPoint

class StationAdapters(
    private val stations :Array<Station>,
    private val mapCtrl: IMapController,
    private var setCenter: (center: GeoPoint, zoom: Int) -> (Boolean)
): RecyclerView.Adapter<StationViewHolder>() {

    override fun getItemCount(): Int {
        return stations.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StationViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val rowForCol = layoutInflater.inflate(R.layout.station_search_item, parent, false)
        return StationViewHolder(rowForCol, 0.00, 0.00)
    }

    override fun onBindViewHolder(holder: StationViewHolder, position: Int) {
        val station = stations[position]
        holder.itemView.stationName.text = station.name

        holder.lat = station.position.latitude
        holder.lon = station.position.longitude
        holder.mapCtrl = mapCtrl
        holder.setCenter = setCenter
    }
}

class StationViewHolder(
    v: View,
    var lat: Double,
    var lon: Double,
    var setCenter: ((center: GeoPoint, zoom: Int) -> (Boolean)) ? = null,
    var mapCtrl: IMapController ? = null
): RecyclerView.ViewHolder(v) {
    init {
        v.selectStation.setOnClickListener {
            val center = GeoPoint(lat, lon)
            setCenter?.invoke(center, 19)

        }
    }
}