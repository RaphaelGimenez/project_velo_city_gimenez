package com.example.projet_gimenez.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.projet_gimenez.R
import com.example.projet_gimenez.models.Contract
import kotlinx.android.synthetic.main.card_contract_item.view.*

class ContractAdapters(
    private val contracts: Array<Contract>,
    private var getStations: (cityName: String) -> (Boolean)
) : RecyclerView.Adapter<CustomViewHolder>() {

    override fun getItemCount(): Int {
        return contracts.count()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val contractForGrid = layoutInflater.inflate(R.layout.card_contract_item, parent, false)
        return CustomViewHolder(contractForGrid)
    }

    override fun onBindViewHolder(holder: CustomViewHolder, position: Int) {
        val contract = contracts[position]
        holder.itemView.city.text = contract.name.capitalize()
        holder.itemView.name.text = contract.commercial_name

        holder.city = contract.name
        holder.getStations = getStations
    }
}

class CustomViewHolder(
    v: View,
    var getStations: ((cityName: String) -> (Boolean))? = null,
    var city: String = ""
) : RecyclerView.ViewHolder(v) {

    companion object {
        const val CITY_NAME = "city"
        const val STATIONS = "stations"
    }

    init {
        v.button.setOnClickListener {
            getStations?.invoke(city)
        }
    }
}