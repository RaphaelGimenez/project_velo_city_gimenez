package com.example.projet_gimenez.models

class Station(
    val name: String,
    val number: Int,
    val contractName: String,
    val address: String,
    val position: StationPosition,
    val banking: Boolean,
    val status: String,
    val lastUpdate: String,
    val totalStands: TotalStands
)

class StationPosition(
    val stationName: String,
    val latitude: Double,
    val longitude: Double
)

class TotalStands(
    val stationName: String,
    val availabilities: Availabilities,
    val capacity: Int
)

class Availabilities(
    val bikes: Int,
    val stands: Int,
    val mechanicalBikes: Int,
    val electricalBikes: Int,
    val electricalInternalBatteryBikes: Int,
    val electricalRemovableBatteryBikes: Int
)