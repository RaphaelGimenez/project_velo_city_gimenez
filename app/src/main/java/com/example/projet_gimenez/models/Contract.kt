package com.example.projet_gimenez.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Contract(
    @PrimaryKey
    val name: String,
    val commercial_name: String,
    val cities: Array<String> ?
)
