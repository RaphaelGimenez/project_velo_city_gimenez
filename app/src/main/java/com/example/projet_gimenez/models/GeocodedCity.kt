package com.example.projet_gimenez.models

class GeocodedCity(
    val lat: Double,
    val lon: Double,
    val boundingbox: Array<Double>
)