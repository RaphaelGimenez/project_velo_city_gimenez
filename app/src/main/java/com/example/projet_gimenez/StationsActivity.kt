package com.example.projet_gimenez

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.projet_gimenez.adapters.CustomViewHolder
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.projet_gimenez.adapters.StationAdapters
import com.example.projet_gimenez.models.GeocodedCity
import com.example.projet_gimenez.models.Station
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.activity_sations.*
import okhttp3.*
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.util.BoundingBox
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import java.io.File
import java.io.IOException
import android.preference.PreferenceManager
import com.example.projet_gimenez.utils.Utils


class StationsActivity : AppCompatActivity() {

    private lateinit var mapView: MapView
    private lateinit var mapViewController: IMapController
    private var cityName: String? = ""
    private var stations: Array<Station> = arrayOf()
    private var filteredStations: Array<Station> = arrayOf()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //load/initialize the osmdroid configuration, this can be done
        val ctx = applicationContext
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx))

        setContentView(R.layout.activity_sations)

        val osmConfig = Configuration.getInstance()
        osmConfig.userAgentValue = packageName
        val basePath = File(cacheDir.absolutePath, "osmdroid")
        osmConfig.osmdroidBasePath = basePath
        val tileCache = File(osmConfig.osmdroidBasePath, "tile")
        osmConfig.osmdroidTileCache = tileCache

        cityName = intent.getStringExtra(CustomViewHolder.CITY_NAME)
        supportActionBar?.title = cityName?.capitalize()

        val stringStations = intent.getStringExtra(CustomViewHolder.STATIONS)

        val gson = GsonBuilder().create()
        stations = gson.fromJson(stringStations, Array<Station>::class.java)

        mapView = findViewById(R.id.mapview)
        mapView.isClickable = true
        mapView.setBuiltInZoomControls(true)
        mapView.setMultiTouchControls(true)
        //Set below to False to check if it works offline
        mapView.setUseDataConnection(true)

        mapView.setTileSource(TileSourceFactory.DEFAULT_TILE_SOURCE)


        mapViewController = mapView.controller
        mapViewController.setZoom(16)
        getCityCoordinates()

        recyclerView_station.layoutManager = LinearLayoutManager(this)
        recyclerView_station.visibility = View.INVISIBLE

        searchStation.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                recyclerView_station.visibility = View.VISIBLE
            } else {
                recyclerView_station.visibility = View.INVISIBLE
            }
        }

        searchStation.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {

                val newFilteredStations = stations
                    .filter { it.name.contains(s, ignoreCase = true) }

                filteredStations = newFilteredStations.toTypedArray()

                runOnUiThread {
                    recyclerView_station.adapter =
                        StationAdapters(filteredStations, mapViewController, ::setCenter)
                }

                println(s)
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
    }

    private fun getCityCoordinates() {
        val url = "https://nominatim.openstreetmap.org/search?city=$cityName&format=json"

        val request = Request.Builder().url(url).header("User-Agent", "project_dawin_gimenez").build()

        val client = OkHttpClient()

        println("newCall")
        client.newCall(request).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                val bodyContent = response.body?.string()

                if (bodyContent !== null) {
                    println(bodyContent)
                    val gson = GsonBuilder().create()
                    val results = gson.fromJson(bodyContent, Array<GeocodedCity>::class.java)
                    val city = results[0]

                    Utils.writeToJSONFile(applicationContext, "project", "Geocoded_$cityName", gson.toJson(city))

                    setMap(city)
                }
            }

            override fun onFailure(call: Call, e: IOException) {
                println("No internet Access")
                val readData = Utils.readFromJSONFile(applicationContext,"project", "Geocoded_$cityName")

                val gson = GsonBuilder().create()
                val geocodedCity = gson.fromJson(readData, GeocodedCity::class.java)
                setMap(geocodedCity)
            }
        })
    }

    private fun setMap(city: GeocodedCity) {
        val center = GeoPoint(city.lat, city.lon)
        val boundingBox = BoundingBox(
            city.boundingbox[1],
            city.boundingbox[3],
            city.boundingbox[0],
            city.boundingbox[2]
        )

        runOnUiThread {
            mapViewController.setCenter(center)
            mapView.zoomToBoundingBox(boundingBox, true)
            recyclerView_station.adapter =
                StationAdapters(filteredStations, mapViewController, ::setCenter)
            setMarkers()
        }
    }

    private fun setMarkers() {
        for (station: Station in stations) {
            val markerPos = GeoPoint(station.position.latitude, station.position.longitude)
            val marker = Marker(mapView)
            marker.position = markerPos

            val percentFilled =
                station.totalStands.availabilities.bikes * 100 / station.totalStands.capacity

            marker.textLabelForegroundColor = Color.BLACK
            when {
                percentFilled > 66 -> {
                    marker.textLabelBackgroundColor = Color.GREEN
                }
                percentFilled in 33..66 -> {
                    marker.textLabelBackgroundColor = Color.YELLOW
                }
                else -> {
                    marker.textLabelBackgroundColor = Color.RED
                    marker.textLabelForegroundColor = Color.WHITE
                }
            }

            marker.textLabelFontSize = 28
            marker.title = "${station.name} : ${station.status}"
            marker.snippet = "${station.totalStands.availabilities.bikes} " +
                    "vélos disponibles sur ${station.totalStands.capacity} " +
                    "dont ${station.totalStands.availabilities.electricalBikes} électriques"
            marker.subDescription = "dernière mise à jour : ${station.lastUpdate}"
            marker.setTextIcon("${station.totalStands.availabilities.bikes}/${station.totalStands.capacity.toString()}")
            marker.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            mapView.overlays.add(marker)
        }
    }

    fun setCenter(center: GeoPoint, zoom: Int): Boolean {
        recyclerView_station.visibility = View.INVISIBLE
        searchStation.clearFocus()
        mapViewController.setCenter(center)
        mapViewController.zoomTo(zoom)
        return true
    }

}